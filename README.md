# Automação de testes back-end - 'ViaCEP' 

## Como executar
1. Clone este projeto com opção HTTPS ou SSH.

2. Após clonado, abra o arquivo .env presente na raíz e escolha o CEP a ser buscado (existente/inexistente) e salve-o.

3. Para execução direta e conferência do report gerado, certifique-se de ter o maven presente em seu SO.

4. Com maven instalado/configurado, acesse a pasta do projeto pelo terminal e execute o comando: 
   mvn clean test site
   
5. Os testes serão executados automaticamente e o report estará presente em target\site\index.html, no link 'Project Reports'.

Aprecie!