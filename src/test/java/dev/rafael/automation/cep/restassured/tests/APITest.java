package dev.rafael.automation.cep.restassured.tests;

import io.github.cdimascio.dotenv.Dotenv;
import io.restassured.RestAssured;

import org.hamcrest.CoreMatchers;
import org.junit.Test;


public class APITest {
	// Invokes the *.env file of ZIP code chosen 
	Dotenv dotenv = Dotenv.load();
	// Storages the url and route from requisition
	private final String BASE_URI = "https://viacep.com.br/ws/";
	private final String ROUTE = "/json";
	
	@Test
	public void validZipCode() {
	// Loads the valid ZIP code
	final String validCep = dotenv.get("VALID_CEP");

		RestAssured.given()
		// Executes the requisition
		.when()
			.get(BASE_URI+validCep+ROUTE)
		.then() 	
			// Does the response validation
            .statusCode(200)
            .body("cep", CoreMatchers.is(validCep))
            .body("logradouro", CoreMatchers.anything())
            .body("complemento", CoreMatchers.anything())
            .body("bairro", CoreMatchers.anything())
            .body("localidade", CoreMatchers.anything())
            .body("uf", CoreMatchers.anything())
            .body("ibge", CoreMatchers.anything())
            .body("gia", CoreMatchers.anything())
            .body("ddd", CoreMatchers.anything())
            .body("siafi", CoreMatchers.anything())
			.log().all()
        ;
	}
	
	@Test
	public void invalidZipCode() {
	// Loads the invalid ZIP code
	final String invalidCep = dotenv.get("INVALID_CEP");
	
		RestAssured.given()
		// Executes the requisition
		.when()
			.get(BASE_URI+invalidCep+ROUTE)
		.then() 	
			// Does the response validation
			.statusCode(400)
			.log().all()
		;
	}
}
